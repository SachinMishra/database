package com.sachin.database1;

/**
 * Created by DELL on 14-08-2018.
 */

public class CriminalRecord
{

    int id;
    String name,cases,desp;   //getter and setter method is used to app with mediator, to reduce the complexity
                             //otherwise every activity go to database which makes heavy for our app.

    public int getId()

    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCases()
    {
        return cases;
    }

    public void setCases(String cases)
    {
        this.cases = cases;
    }

    public String getDesp()
    {
        return desp;
    }

    public void setDesp(String desp)
    {
        this.desp = desp;
    }



    public CriminalRecord(int id, String name, String cases, String desp)  //called from MainActivity.java(when create the object
                                                                            // of this class(CriminalRecord))
    {
        this.id = id;
        this.name = name;
        this.cases = cases;
        this.desp = desp;
    }


    public CriminalRecord()
    {


    }


}
