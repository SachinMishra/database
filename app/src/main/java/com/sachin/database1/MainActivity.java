package com.sachin.database1;

import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import static com.sachin.database1.R.id.editText2;
import static com.sachin.database1.R.id.editText4;

public class MainActivity extends AppCompatActivity
{

    EditText e1,e2,e3,e4;
    Button b1,b2,b3,b4,b5;

    My_database db;

    @Override

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        e1= (EditText) findViewById(editText2);
        e2= (EditText) findViewById(R.id.editText3);
        e3= (EditText) findViewById(editText4);
        e4= (EditText) findViewById(R.id.editText5);
        b1= (Button) findViewById(R.id.button);
        b2= (Button) findViewById(R.id.button2);
        b3= (Button) findViewById(R.id.button3);
        b4= (Button) findViewById(R.id.button4);
        b5= (Button) findViewById(R.id.button5);

        db=new My_database(this);

        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                int id=Integer.parseInt(e1.getText().toString());
                String name=e2.getText().toString();
                String cases=e3.getText().toString();
                String desc=e4.getText().toString();

                CriminalRecord record=new CriminalRecord(id,name,cases,desc);

                db.addRecord(record);   // called the record method in My_database.java,which added values in DataBase

                                        //record contains reference of CriminalRecord class
                e1.setText("");
                e2.setText("");
                e3.setText("");
                e4.setText("");

                Toast.makeText(MainActivity.this, "Data Saved", Toast.LENGTH_SHORT).show();

            }
        });

        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String data=e1.getText().toString();

                if(data.equals(""))
                {
                    e1.setError("plz enter ID");
                }
                else
                {
                    int id=Integer.parseInt(data);

                   CriminalRecord record=db.getSingleRecord(id);  //here getSingleRecord(id) is called

                  Toast.makeText(MainActivity.this, "ID -"+record.getId()+"\nname-"+record.getName()
                           +"\nCases- "+record.getCases()+"\n desp "+record.getDesp(),Toast.LENGTH_SHORT).show();

                 /*  e1.setText(data);
                    e2.setText(record.getName());
                    e3.setText(record.getCases());
                    e4.setText(record.getDesp());
                  */

                }


            }
        });


        b3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                List<CriminalRecord> list=db.getAllRecord();


                for(CriminalRecord record:list)
                {
                    Toast.makeText(MainActivity.this, "ID -"+record.getId()+"\nname-"+record.getName()
                            +"\nCases- "+record.getCases()+"\n desp "+record.getDesp(),Toast.LENGTH_SHORT).show();


                }

            }
        });

        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                int id=Integer.parseInt(e1.getText().toString());
                String name=e2.getText().toString();
                String cases=e3.getText().toString();
                String desc=e4.getText().toString();

                CriminalRecord record=new CriminalRecord();

                record.setId(id);
                record.setName(name);
                record.setCases(cases);
                record.setDesp(desc);
                db.updateRecord(record);

                e1.setText("");
                e2.setText("");
                e3.setText("");
                e4.setText("");

                Toast.makeText(MainActivity.this, "Data updated", Toast.LENGTH_SHORT).show();

            }
        });


        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String data=e1.getText().toString();

                if(data.equals(""))
                {
                    e1.setError("plz enter id");
                }
                else
                {
                    int id=Integer.parseInt(data);

                    db.deleteRecord(id);

                    e1.setText("");
                    Toast.makeText(MainActivity.this, "Record Deleted", Toast.LENGTH_SHORT).show();

                }

            }
        });


    }
}
