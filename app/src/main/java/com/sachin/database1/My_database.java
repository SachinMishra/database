package com.sachin.database1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 14-08-2018.
 */

public class My_database extends SQLiteOpenHelper
{

    public static final int DATABASE_VERSION=1;  //here we are making final becoz we not to change column name,Databade name etc.

    public static final String DATABASE_NAME="MyDb";

    public static final String TABLE_NAME="criminal_record";

    public static final String ID ="id";
    public static final String NAME="name";
    public static final String CASES="cases";
    public static final String DESC="desc";




    public My_database(Context context)
    {

        super(context,DATABASE_NAME,null,DATABASE_VERSION);  // parameters are deleted because operating system pass its own

           //here database is created.                           // parameters but we make database acc. to our requirement
    }



    @Override
    public void onCreate(SQLiteDatabase db)  //call when create the Table one time
    {


        String query="CREATE TABLE " +TABLE_NAME+"("+ID+" NUMBER PRIMARY KEY,"+NAME+" TEXT,"+CASES+" TEXT," +DESC+" TEXT);";

         db.execSQL(query);

    }

    @Override

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

     db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);     //when upgrade the table, means changing the name,or column.
        onCreate(db);                                   // when this will execute, means new Database is created and paste on
                                                        // previous database.

    }

    //For Adding the Record

    public void addRecord(CriminalRecord record)
    {
       SQLiteDatabase db= getWritableDatabase();

        ContentValues values=new ContentValues();

      //ContentValues objects are used to insert new rows into database tables (and Content Providers).
        // Each Content Values object represents a single row, as a map of column names to values and
        // it is class, stores the value in form of (Key and Value) pair

        values.put(ID,record.getId());

        values.put(NAME,record.getName());
        //we can also write .. values.put("name",record.getName()); becoz Name consist the String "name".

        values.put(CASES,record.getCases());
        values.put(DESC,record.getDesp());


        db.insert(TABLE_NAME,null,values);


    }

    // for getting the Single Record

    public CriminalRecord getSingleRecord(int id)
    {
     SQLiteDatabase db= getReadableDatabase();

     Cursor cursor= db.query(TABLE_NAME,new String[]{ID,NAME,CASES,DESC},ID+"=?",new String[]{String.valueOf(id)},null,null,null);

        if (cursor!=null)

            cursor.moveToFirst();

  //   Queries in Android are returned as Cursor objects. Rather than extracting and returning a copy of the result values,
  // Cursors act as pointers to a subset of the underlying data. Cursors are a managed way of controlling your position (row)
  // Zin the result set of a database query.

     return new CriminalRecord(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3));
    }

    // for Getting the Complete Record

    public ArrayList<CriminalRecord> getAllRecord()   //ArayList is used to show data from Database.
    {
        SQLiteDatabase db= getReadableDatabase();

        ArrayList<CriminalRecord> list=new ArrayList<CriminalRecord>();  //Array show the CriminalRecord type data,

        String query="SELECT * FROM "+TABLE_NAME;

        Cursor cursor=db.rawQuery(query,null);

        if(cursor.moveToFirst())
        {
            do
            {
                CriminalRecord record=new CriminalRecord(); // call the default constructor which is in CriminaRecord.java

                record.setId(cursor.getInt(0));            //data is stored in form of object in record variable.
                record.setName(cursor.getString(1));
                record.setCases(cursor.getString(2));
                record.setDesp(cursor.getString(3));

                //add all record to list

                list.add(record);

            }
            while (cursor.moveToNext());

        }
        return list;


    }

    //for Updating the Record

    public  void updateRecord(CriminalRecord record)
    {

      SQLiteDatabase db=getWritableDatabase();

        ContentValues values=new ContentValues();

        values.put(NAME,record.getName());
        values.put(CASES,record.getCases());
        values.put(DESC,record.getDesp());



        db.update(TABLE_NAME,values,ID+"=?",new String[]{String.valueOf(record.getId())});


    }

    //For Deleting the Record

    public void deleteRecord(int id)
    {
        SQLiteDatabase db=getWritableDatabase();
     db.delete(TABLE_NAME,ID+"=?",new String[]{String.valueOf(id)});

        db.close();


    }

}



